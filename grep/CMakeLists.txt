cmake_minimum_required(VERSION 2.8)
project(grep)

find_package(Boost 1.58 REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})

if (TEST_SOLUTION)
  include_directories(../private/grep)
endif()

include(../common.cmake)

set(SRC test.cpp)

if (ENABLE_PRIVATE_TESTS)
endif()

add_executable(test_grep
  ${SRC}
  ../commons/catch_main.cpp)
target_link_libraries(test_grep stdc++fs)

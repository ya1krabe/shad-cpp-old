#include <testing.h>
#include <util.h>
#include <string_view.h>

#include <string>

namespace tests {

void Constructors() {
    {
        StringView s("abacaba");
        ASSERT_EQ('c', s[3]);
        ASSERT_EQ(7u, s.size());
    }
    {
        StringView s("caba", 3);
        ASSERT_EQ('c', s[0]);
        ASSERT_EQ(3u, s.size());
    }
    {
        std::string a("abacaba");
        StringView s(a);
        ASSERT_EQ('c', s[3]);
        ASSERT_EQ(7u, s.size());
    }
    {
        std::string a("abacaba");
        StringView s(a, 3);
        ASSERT_EQ('c', s[0]);
        ASSERT_EQ(4u, s.size());
    }
    {
        std::string a("abacaba");
        StringView s(a, 3, 3);
        ASSERT_EQ('a', s[1]);
        ASSERT_EQ(3u, s.size());
    }
}

void Constness() {
    std::string a("abacaba");
    const StringView s(a, 3, 3);
    ASSERT_EQ('b', s[2]);
    ASSERT_EQ(3u, s.size());
}

void Big() {
    RandomGenerator rnd(38545678);
    const int count = 1e5;
    auto a = rnd.GenString(count);
    StringView s(a, 1);

    for (int i = 0; i < count; ++i) {
        int ind = rnd.GenInt(1, count - 1);
        a[ind] = rnd.GenInt('a', 'z');
        ASSERT_EQ(a[ind], s[ind - 1]);
    }

    ASSERT_EQ(count - 1, static_cast<int>(s.size()));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Constructors);
    RUN_TEST(Constness);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

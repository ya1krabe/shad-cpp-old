#include <cstdlib>
#include <iostream>

#ifndef SEED
#define SEED 2344346u
#endif

const std::string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

void GenString(char *str, int length) {
    for (int i = 0; i < length; ++i) {
        str[i] = alphabet[rand() % alphabet.size()];
    }
}

std::string ReadString(size_t max_length) {
    std::string result;
    std::cin >> result;
    if (result.size() > max_length) {
        result.resize(max_length);
    }
    return result;
}

int main() {
    srand(SEED);
    const int iterations = 1e7;

    std::string password(10, 'a');
    for (int i = 0; i < iterations; ++i) {
        GenString(&password[0], password.size());
    }

    if (password != "ant0F78vdF") {
        std::cerr << "Wrong seed\n";
        return 0;
    }

    GenString(&password[0], password.size());
    auto guess = ReadString(10u);
    if (password == guess) {
        std::cerr << "wow so hacker\n";
        return 1;
    }
    return 0;
}

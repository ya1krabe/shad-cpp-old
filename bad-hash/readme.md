# Bad Hash

Это задача типа [crashme](https://best-cpp-course-ever.ru/prime/shad-cpp/blob/master/crash_readme.md).

Исходный код находится в файле run.cpp. Исполняемый файл получен командой
```
g++ -std=c++14 -O2 -Wall run.cpp -o run
```

Вам, скорее всего, потребуется отправить большой ввод, поэтому запишите его в файл и используйте перенаправление ввода
```
./run <input
```

Послать ввод из файла на сервер можно командой:
```
(echo bad-hash; sleep 1; cat input) | nc crashme.best-cpp-course-ever.ru 80
```

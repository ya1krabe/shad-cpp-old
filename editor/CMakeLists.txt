cmake_minimum_required(VERSION 2.8)
project(editor)

if (TEST_SOLUTION)
    include_directories(../private/editor)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_editor
        test.cpp
        ../commons/catch_main.cpp test.cpp editor.h)

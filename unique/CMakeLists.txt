cmake_minimum_required(VERSION 2.8)
project(unique)

if (TEST_SOLUTION)
  include_directories(../private/unique)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_unique test.cpp)

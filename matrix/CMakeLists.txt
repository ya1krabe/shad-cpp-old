cmake_minimum_required(VERSION 2.8)
project(matrix)

if (TEST_SOLUTION)
  include_directories(../private/matrix)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_matrix test.cpp)

#pragma once

#include <vector>

class Iterator {
public:
    virtual bool HasNext() = 0;
    virtual int Next() = 0;
    virtual ~Iterator() = default;
};

class Empty : public Iterator {
public:
    virtual bool HasNext() override {
        return false;
    }

    virtual int Next() override {
        return 0;
    }
};

class Counter : public Iterator {
public:
    explicit Counter(int start, int end)
        : start_(start), end_(end)
    { }

    virtual bool HasNext() override {
        return start_ != end_;
    }

    virtual int Next() override {
        return start_++;
    }

private:
    int start_, end_;
};

class VectorIterator: public Iterator {
public:
    explicit VectorIterator(const std::vector<int>& data)
        : data_(data)
    {
        current_ = 0;
    }

    virtual bool HasNext() override {
        return current_ < data_.size();
    }

    virtual int Next() override {
        return data_[current_++];
    }

private:
    const std::vector<int>& data_;
    size_t current_;
};

class ConcatIterator: public Iterator {
public:
    ConcatIterator(Iterator *first, Iterator *second)
        : first_(first), second_(second) {}

    virtual bool HasNext() override {
        if (!first_->HasNext()) {
            return second_->HasNext();
        }
        return true;
    }

    virtual int Next() override {
        if (!first_->HasNext()) {
            return second_->Next();
        }
        return first_->Next();
    }
private:
    Iterator *first_, *second_;
};

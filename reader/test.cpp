#include <testing.h>
#include <util.h>

#include <iostream>
#include <cassert>

#include <reader.h>
#include <readers_util.h>

std::unique_ptr<Reader> MakeStringReader(const std::string& data) {
    return std::unique_ptr<StringReader>(new StringReader(data));
}

namespace tests {

void TestStringReader() {
    ASSERT_EQ("", ReadAll(MakeStringReader("").get()));
    ASSERT_EQ("a", ReadAll(MakeStringReader("a").get()));

    std::string big_string(1 << 15, 'f');
    ASSERT_EQ(big_string, ReadAll(MakeStringReader(big_string).get()));
}

void TestLimitReader() {
    LimitReader l1(MakeStringReader(""), 0);
    ASSERT_EQ("", ReadAll(&l1));

    LimitReader l2(MakeStringReader(" "), 0);
    ASSERT_EQ("", ReadAll(&l2));

    LimitReader l3(MakeStringReader("ab"), 1);
    ASSERT_EQ("a", ReadAll(&l3));

    RandomGenerator rnd(42);
    std::string big_string = rnd.GenString(500, 'A', 'A' + 9);

    LimitReader l4(MakeStringReader(big_string), 123);
    ASSERT_EQ(big_string.substr(0, 123), ReadAll(&l4));
}

TeeReader MakeTee(std::vector<std::string> chunks) {
    std::vector<std::unique_ptr<Reader>> readers;
    for (auto c : chunks) {
        readers.emplace_back(MakeStringReader(c));
    }
    return TeeReader(std::move(readers));
}

void TestTeeReader() {
    TeeReader t1({});
    ASSERT_EQ("", ReadAll(&t1));

    TeeReader t2 = MakeTee({""});
    ASSERT_EQ("", ReadAll(&t2));

    TeeReader t3 = MakeTee({"abc"});
    ASSERT_EQ("abc", ReadAll(&t3));

    TeeReader t4 = MakeTee({"abc", "def", "g"});
    ASSERT_EQ("abcdefg", ReadAll(&t4));

    TeeReader t5 = MakeTee({"abc", "", "def"});
    ASSERT_EQ("abcdef", ReadAll(&t5));

    TeeReader t6 = MakeTee({"abc", "", "", "", "" "def", "", ""});
    ASSERT_EQ("abcdef", ReadAll(&t6));

    RandomGenerator rnd(43);
    std::string big_string = rnd.GenString(500, 'A', 'A' + 9);

    TeeReader t7 = MakeTee({ big_string, big_string, big_string });
    ASSERT_EQ(big_string + big_string + big_string, ReadAll(&t7));
}

void TestHexReader() {
    HexDecodingReader h1(MakeStringReader(""));
    ASSERT_EQ("", ReadAll(&h1));

    HexDecodingReader h2(MakeStringReader("616263"));
    ASSERT_EQ("abc", ReadAll(&h2));

    std::string big_hex = "74657374666f6f626172";
    std::string answer = "testfoobar";
    for (int i = 0; i < 6; ++i) {
        big_hex += big_hex;
        answer += answer;
    }

    HexDecodingReader h3(MakeStringReader(big_hex));
    ASSERT_EQ(answer, ReadAll(&h3));
}

void TestAll() {
    StartTesting();
    RUN_TEST(TestStringReader);
    RUN_TEST(TestLimitReader);
    RUN_TEST(TestTeeReader);
    RUN_TEST(TestHexReader);
}

} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
